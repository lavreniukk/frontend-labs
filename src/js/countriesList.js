/* eslint-disable no-unused-vars */
/* eslint-disable comma-dangle */
/* eslint-disable quotes */
/* eslint-disable indent */
// eslint-disable-next-line import/prefer-default-export
export const countriesList = [
    {
        country: "Afghanistan",
        location: "Asia"
    },
    {
        country: "Albania",
        location: "Europe"
    },
    {
        country: "Algeria",
        location: "Africa"
    },
    {
        country: "American Samoa",
        location: "Oceania"
    },
    {
        country: "Andorra",
        location: "Europe"
    },
    {
        country: "Angola",
        location: "Africa"
    },
    {
        country: "Anguilla",
        location: "South America"
    },
    {
        country: "Antarctica",
        location: "Antarctica"
    },
    {
        country: "Antigua and Barbuda",
        location: "South America"
    },
    {
        country: "Argentina",
        location: "South America"
    },
    {
        country: "Armenia",
        location: "Asia"
    },
    {
        country: "Aruba",
        location: "South America"
    },
    {
        country: "Australia",
        location: "Australia"
    },
    {
        country: "Austria",
        location: "Europe"
    },
    {
        country: "Azerbaijan",
        location: "Asia"
    },
    {
        country: "Bahamas",
        location: "South America"
    },
    {
        country: "Bahrain",
        location: "Asia"
    },
    {
        country: "Bangladesh",
        location: "Asia"
    },
    {
        country: "Barbados",
        location: "South America"
    },
    {
        country: "Belarus",
        location: "Europe"
    },
    {
        country: "Belgium",
        location: "Europe"
    },
    {
        country: "Belize",
        location: "North America"
    },
    {
        country: "Benin",
        location: "Africa"
    },
    {
        country: "Bermuda",
        location: "North America"
    },
    {
        country: "Bhutan",
        location: "Asia"
    },
    {
        country: "Bolivia",
        location: "South America"
    },
    {
        country: "Bosnia and Herzegovina",
        location: "Europe"
    },
    {
        country: "Botswana",
        location: "Africa"
    },
    {
        country: "Bouvet Island",
        location: "Antarctica"
    },
    {
        country: "Brazil",
        location: "South America"
    },
    {
        country: "British Indian Ocean Territory",
        location: "Africa"
    },
    {
        country: "Brunei",
        location: "Asia"
    },
    {
        country: "Bulgaria",
        location: "Europe"
    },
    {
        country: "Burkina Faso",
        location: "Africa"
    },
    {
        country: "Burundi",
        location: "Africa"
    },
    {
        country: "Cambodia",
        location: "Asia"
    },
    {
        country: "Cameroon",
        location: "Africa"
    },
    {
        country: "Canada",
        location: "North America"
    },
    {
        country: "Cape Verde",
        location: "Africa"
    },
    {
        country: "Cayman Islands",
        location: "South America"
    },
    {
        country: "Central African Republic",
        location: "Africa"
    },
    {
        country: "Chad",
        location: "Africa"
    },
    {
        country: "Chile",
        location: "South America"
    },
    {
        country: "China",
        location: "Asia"
    },
    {
        country: "Christmas Island",
        location: "Australia"
    },
    {
        country: "Cocos (Keeling) Islands",
        location: "Australia"
    },
    {
        country: "Colombia",
        location: "South America"
    },
    {
        country: "Comoros",
        location: "Africa"
    },
    {
        country: "Congo",
        location: "Africa"
    },
    {
        country: "Cook Islands",
        location: "Oceania"
    },
    {
        country: "Costa Rica",
        location: "North America"
    },
    {
        country: "Croatia",
        location: "Europe"
    },
    {
        country: "Cuba",
        location: "South America"
    },
    {
        country: "Cyprus",
        location: "Asia"
    },
    {
        country: "Czech Republic",
        location: "Europe"
    },
    {
        country: "Denmark",
        location: "Europe"
    },
    {
        country: "Djibouti",
        location: "Africa"
    },
    {
        country: "Dominica",
        location: "South America"
    },
    {
        country: "Dominican Republic",
        location: "South America"
    },
    {
        country: "East Timor",
        location: "Asia"
    },
    {
        country: "Ecuador",
        location: "South America"
    },
    {
        country: "Egypt",
        location: "Africa"
    },
    {
        country: "El Salvador",
        location: "North America"
    },
    {
        country: "England",
        location: "Europe"
    },
    {
        country: "Equatorial Guinea",
        location: "Africa"
    },
    {
        country: "Eritrea",
        location: "Africa"
    },
    {
        country: "Estonia",
        location: "Europe"
    },
    {
        country: "Ethiopia",
        location: "Africa"
    },
    {
        country: "Falkland Islands",
        location: "South America"
    },
    {
        country: "Faroe Islands",
        location: "Europe"
    },
    {
        country: "Fiji Islands",
        location: "Oceania"
    },
    {
        country: "Finland",
        location: "Europe"
    },
    {
        country: "France",
        location: "Europe"
    },
    {
        country: "French Guiana",
        location: "South America"
    },
    {
        country: "French Polynesia",
        location: "Oceania"
    },
    {
        country: "French Southern territories",
        location: "Antarctica"
    },
    {
        country: "Gabon",
        location: "Africa"
    },
    {
        country: "Gambia",
        location: "Africa"
    },
    {
        country: "Georgia",
        location: "Asia"
    },
    {
        country: "Germany",
        location: "Europe"
    },
    {
        country: "Ghana",
        location: "Africa"
    },
    {
        country: "Gibraltar",
        location: "Europe"
    },
    {
        country: "Greece",
        location: "Europe"
    },
    {
        country: "Greenland",
        location: "North America"
    },
    {
        country: "Grenada",
        location: "South America"
    },
    {
        country: "Guadeloupe",
        location: "South America"
    },
    {
        country: "Guam",
        location: "Oceania"
    },
    {
        country: "Guatemala",
        location: "North America"
    },
    {
        country: "Guinea",
        location: "Africa"
    },
    {
        country: "Guinea-Bissau",
        location: "Africa"
    },
    {
        country: "Guyana",
        location: "South America"
    },
    {
        country: "Haiti",
        location: "South America"
    },
    {
        country: "Heard Island and McDonald Islands",
        location: "Antarctica"
    },
    {
        country: "Holy See (Vatican City State)",
        location: "Europe"
    },
    {
        country: "Honduras",
        location: "North America"
    },
    {
        country: "Hong Kong",
        location: "Asia"
    },
    {
        country: "Hungary",
        location: "Europe"
    },
    {
        country: "Iceland",
        location: "Europe"
    },
    {
        country: "India",
        location: "Asia"
    },
    {
        country: "Indonesia",
        location: "Asia"
    },
    {
        country: "Iran",
        location: "Asia"
    },
    {
        country: "Iraq",
        location: "Asia"
    },
    {
        country: "Ireland",
        location: "Europe"
    },
    {
        country: "Israel",
        location: "Asia"
    },
    {
        country: "Italy",
        location: "Europe"
    },
    {
        country: "Ivory Coast",
        location: "Africa"
    },
    {
        country: "Jamaica",
        location: "South America"
    },
    {
        country: "Japan",
        location: "Asia"
    },
    {
        country: "Jordan",
        location: "Asia"
    },
    {
        country: "Kazakhstan",
        location: "Asia"
    },
    {
        country: "Kenya",
        location: "Africa"
    },
    {
        country: "Kiribati",
        location: "Oceania"
    },
    {
        country: "Kuwait",
        location: "Asia"
    },
    {
        country: "Kyrgyzstan",
        location: "Asia"
    },
    {
        country: "Laos",
        location: "Asia"
    },
    {
        country: "Latvia",
        location: "Europe"
    },
    {
        country: "Lebanon",
        location: "Asia"
    },
    {
        country: "Lesotho",
        location: "Africa"
    },
    {
        country: "Liberia",
        location: "Africa"
    },
    {
        country: "Libyan Arab Jamahiriya",
        location: "Africa"
    },
    {
        country: "Liechtenstein",
        location: "Europe"
    },
    {
        country: "Lithuania",
        location: "Europe"
    },
    {
        country: "Luxembourg",
        location: "Europe"
    },
    {
        country: "Macao",
        location: "Asia"
    },
    {
        country: "North Macedonia",
        location: "Europe"
    },
    {
        country: "Madagascar",
        location: "Africa"
    },
    {
        country: "Malawi",
        location: "Africa"
    },
    {
        country: "Malaysia",
        location: "Asia"
    },
    {
        country: "Maldives",
        location: "Asia"
    },
    {
        country: "Mali",
        location: "Africa"
    },
    {
        country: "Malta",
        location: "Europe"
    },
    {
        country: "Marshall Islands",
        location: "Oceania"
    },
    {
        country: "Martinique",
        location: "South America"
    },
    {
        country: "Mauritania",
        location: "Africa"
    },
    {
        country: "Mauritius",
        location: "Africa"
    },
    {
        country: "Mayotte",
        location: "Africa"
    },
    {
        country: "Mexico",
        location: "North America"
    },
    {
        country: "Micronesia, Federated States of",
        location: "Oceania"
    },
    {
        country: "Moldova",
        location: "Europe"
    },
    {
        country: "Monaco",
        location: "Europe"
    },
    {
        country: "Mongolia",
        location: "Asia"
    },
    {
        country: "Montserrat",
        location: "South America"
    },
    {
        country: "Morocco",
        location: "Africa"
    },
    {
        country: "Mozambique",
        location: "Africa"
    },
    {
        country: "Myanmar",
        location: "Asia"
    },
    {
        country: "Namibia",
        location: "Africa"
    },
    {
        country: "Nauru",
        location: "Oceania"
    },
    {
        country: "Nepal",
        location: "Asia"
    },
    {
        country: "Netherlands",
        location: "Europe"
    },
    {
        country: "Netherlands Antilles",
        location: "South America"
    },
    {
        country: "New Caledonia",
        location: "Oceania"
    },
    {
        country: "New Zealand",
        location: "Australia"
    },
    {
        country: "Nicaragua",
        location: "North America"
    },
    {
        country: "Niger",
        location: "Africa"
    },
    {
        country: "Nigeria",
        location: "Africa"
    },
    {
        country: "Niue",
        location: "Oceania"
    },
    {
        country: "Norfolk Island",
        location: "Australia"
    },
    {
        country: "North Korea",
        location: "Asia"
    },
    {
        country: "Northern Ireland",
        location: null
    },
    {
        country: "Northern Mariana Islands",
        location: "Oceania"
    },
    {
        country: "Norway",
        location: "Europe"
    },
    {
        country: "Oman",
        location: "Asia"
    },
    {
        country: "Pakistan",
        location: "Asia"
    },
    {
        country: "Palau",
        location: "Oceania"
    },
    {
        country: "Palestine",
        location: "Asia"
    },
    {
        country: "Panama",
        location: "North America"
    },
    {
        country: "Papua New Guinea",
        location: "Oceania"
    },
    {
        country: "Paraguay",
        location: "South America"
    },
    {
        country: "Peru",
        location: "South America"
    },
    {
        country: "Philippines",
        location: "Asia"
    },
    {
        country: "Pitcairn",
        location: "Oceania"
    },
    {
        country: "Poland",
        location: "Europe"
    },
    {
        country: "Portugal",
        location: "Europe"
    },
    {
        country: "Puerto Rico",
        location: "South America"
    },
    {
        country: "Qatar",
        location: "Asia"
    },
    {
        country: "Reunion",
        location: "Africa"
    },
    {
        country: "Romania",
        location: "Europe"
    },
    {
        country: "Russian Federation",
        location: "Europe"
    },
    {
        country: "Rwanda",
        location: "Africa"
    },
    {
        country: "Saint Helena",
        location: "Africa"
    },
    {
        country: "Saint Kitts and Nevis",
        location: "South America"
    },
    {
        country: "Saint Lucia",
        location: "South America"
    },
    {
        country: "Saint Pierre and Miquelon",
        location: "North America"
    },
    {
        country: "Saint Vincent and the Grenadines",
        location: "South America"
    },
    {
        country: "Samoa",
        location: "Oceania"
    },
    {
        country: "San Marino",
        location: "Europe"
    },
    {
        country: "Sao Tome and Principe",
        location: "Africa"
    },
    {
        country: "Saudi Arabia",
        location: "Asia"
    },
    {
        country: "Scotland",
        location: null
    },
    {
        country: "Senegal",
        location: "Africa"
    },
    {
        country: "Serbia",
        location: "Europe"
    },
    {
        country: "Seychelles",
        location: "Africa"
    },
    {
        country: "Sierra Leone",
        location: "Africa"
    },
    {
        country: "Singapore",
        location: "Asia"
    },
    {
        country: "Slovakia",
        location: "Europe"
    },
    {
        country: "Slovenia",
        location: "Europe"
    },
    {
        country: "Solomon Islands",
        location: "Oceania"
    },
    {
        country: "Somalia",
        location: "Africa"
    },
    {
        country: "South Africa",
        location: "Africa"
    },
    {
        country: "South Georgia and the South Sandwich Islands",
        location: "Antarctica"
    },
    {
        country: "South Korea",
        location: "Asia"
    },
    {
        country: "South Sudan",
        location: "Africa"
    },
    {
        country: "Spain",
        location: "Europe"
    },
    {
        country: "Sri Lanka",
        location: "Asia"
    },
    {
        country: "Sudan",
        location: "Africa"
    },
    {
        country: "Suriname",
        location: "South America"
    },
    {
        country: "Svalbard and Jan Mayen",
        location: "Europe"
    },
    {
        country: "Swaziland",
        location: "Africa"
    },
    {
        country: "Sweden",
        location: "Europe"
    },
    {
        country: "Switzerland",
        location: "Europe"
    },
    {
        country: "Syria",
        location: "Asia"
    },
    {
        country: "Tajikistan",
        location: "Asia"
    },
    {
        country: "Tanzania",
        location: "Africa"
    },
    {
        country: "Thailand",
        location: "Asia"
    },
    {
        country: "The Democratic Republic of Congo",
        location: "Africa"
    },
    {
        country: "Togo",
        location: "Africa"
    },
    {
        country: "Tokelau",
        location: "Oceania"
    },
    {
        country: "Tonga",
        location: "Oceania"
    },
    {
        country: "Trinidad and Tobago",
        location: "South America"
    },
    {
        country: "Tunisia",
        location: "Africa"
    },
    {
        country: "Turkey",
        location: "Asia"
    },
    {
        country: "Turkmenistan",
        location: "Asia"
    },
    {
        country: "Turks and Caicos Islands",
        location: "South America"
    },
    {
        country: "Tuvalu",
        location: "Oceania"
    },
    {
        country: "Uganda",
        location: "Africa"
    },
    {
        country: "Ukraine",
        location: "Europe"
    },
    {
        country: "United Arab Emirates",
        location: "Asia"
    },
    {
        country: "United Kingdom",
        location: "Europe"
    },
    {
        country: "United States",
        location: "North America"
    },
    {
        country: "United States Minor Outlying Islands",
        location: "Oceania"
    },
    {
        country: "Uruguay",
        location: "South America"
    },
    {
        country: "Uzbekistan",
        location: "Asia"
    },
    {
        country: "Vanuatu",
        location: "Oceania"
    },
    {
        country: "Venezuela",
        location: "South America"
    },
    {
        country: "Vietnam",
        location: "Asia"
    },
    {
        country: "Virgin Islands, British",
        location: "South America"
    },
    {
        country: "Virgin Islands, U.S.",
        location: "South America"
    },
    {
        country: "Wales",
        location: "Europe"
    },
    {
        country: "Wallis and Futuna",
        location: "Oceania"
    },
    {
        country: "Western Sahara",
        location: "Africa"
    },
    {
        country: "Yemen",
        location: "Asia"
    },
    {
        country: "Zambia",
        location: "Africa"
    },
    {
        country: "Zimbabwe",
        location: "Africa"
    }
];
