/* eslint-disable no-undef */
/* eslint-disable arrow-parens */
/* eslint-disable comma-dangle */
/* eslint-disable prefer-destructuring */
/* eslint-disable import/extensions */
// eslint-disable-next-line import/extensions
import { renderTopTeachers } from './renderTeachers.js';
import { teachersData } from './formatTeachers.js';
import { renderStats, renderChart } from './sortTeachers.js';

const addTeacherForm = document.forms.addTeacher;
const fullNameErr = document.getElementById('fullname_error');
const specErr = document.getElementById('speciality_error');
const countryErr = document.getElementById('country_error');
const cityErr = document.getElementById('city_error');
const emailErr = document.getElementById('email_error');
const telnumErr = document.getElementById('telnum_error');
const genderErr = document.getElementById('gender_error');
const commentErr = document.getElementById('comment_error');

const validateStringProperty = (teachersProperty) => {
  const firstLetter = teachersProperty.charAt(0);
  if (teachersProperty === '' || typeof teachersProperty !== 'string' || firstLetter !== _.toUpper(firstLetter)) return false;
  return true;
};

// eslint-disable-next-line no-unused-vars
const validateAge = (age) => {
  if (_.isInteger(age)) {
    return true;
  }
  return false;
};

const validatePhoneNumber = (phone) => {
  // eslint-disable-next-line no-useless-escape
  const checkPhoneRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
  if (phone === '') return false;
  if (checkPhoneRegex.test(phone)) return true;
  return false;
};

const validateEmail = (email) => {
  const checkEmailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  if (checkEmailRegex.test(email)) return true;
  return false;
};

const validateTeacher = () => {
  let isValid = true;

  if (!validateStringProperty(addTeacherForm.fullname.value)) {
    addTeacherForm.fullname.style.borderColor = '#cc4836';
    fullNameErr.innerHTML = 'Your name is required and should contain only letters';
    isValid = false;
  } else {
    addTeacherForm.fullname.style.borderColor = '#767676';
    fullNameErr.innerHTML = '';
  }

  if (!validateStringProperty(addTeacherForm.speciality.value)) {
    addTeacherForm.speciality.style.borderColor = '#cc4836';
    specErr.innerHTML = 'Your speciality is required and should start with capital letter';
    isValid = false;
  } else {
    addTeacherForm.speciality.style.borderColor = '#767676';
    specErr.innerHTML = '';
  }

  if (!validateStringProperty(addTeacherForm.country.value)) {
    addTeacherForm.country.style.borderColor = '#cc4836';
    countryErr.innerHTML = 'Your country is required and should start with capital letter';
    isValid = false;
  } else {
    addTeacherForm.country.style.borderColor = '#767676';
    countryErr.innerHTML = '';
  }

  if (!validateStringProperty(addTeacherForm.city.value)) {
    addTeacherForm.city.style.borderColor = '#cc4836';
    cityErr.innerHTML = 'Your city is required and should start with capital letter';
    isValid = false;
  } else {
    addTeacherForm.city.style.borderColor = '#767676';
    cityErr.innerHTML = '';
  }

  if (!validateStringProperty(addTeacherForm.comment.value)) {
    addTeacherForm.comment.style.borderColor = '#cc4836';
    commentErr.innerHTML = 'Your comment is required and should start with capital letter';
    isValid = false;
  } else {
    addTeacherForm.comment.style.borderColor = '#767676';
    commentErr.innerHTML = '';
  }

  if (!validateStringProperty(addTeacherForm.gender.value)) {
    genderErr.innerHTML = 'Your gender is required';
    isValid = false;
  } else {
    genderErr.innerHTML = '';
  }

  if (!validatePhoneNumber(addTeacherForm.telnum.value)) {
    addTeacherForm.telnum.style.borderColor = '#cc4836';
    telnumErr.innerHTML = 'Your phone number is required and should be correct';
    isValid = false;
  } else {
    addTeacherForm.telnum.style.borderColor = '#767676';
    telnumErr.innerHTML = '';
  }

  if (!validateEmail(addTeacherForm.email.value)) {
    addTeacherForm.email.style.borderColor = '#cc4836';
    emailErr.innerHTML = 'Your email is required and should be correct';
    isValid = false;
  } else {
    addTeacherForm.email.style.borderColor = '#767676';
    emailErr.innerHTML = '';
  }

  return isValid;
};

const generateID = () => _.floor(Math.random() * Date.now());
// eslint-disable-next-line max-len
const createTeacherData = (fullname, speciality, country, city, email, phone, bDate, gender, bgColor, note) => {
  const today = new Date();
  const birthday = new Date(`${bDate}`);
  const age = today.getFullYear() - birthday.getFullYear()
  - (today.getMonth() < birthday.getMonth()
  || (today.getMonth() === birthday.getMonth() && today.getDate() < birthday.getDate()));

  const newTeacher = {
    id: generateID(),
    gender,
    name: {
      first: _.split(fullname, ' ', 2)[0],
      last: _.split(fullname, ' ', 2)[1]
    },
    location: {
      city,
      country
    },
    email,
    dob: {
      age,
      date: bDate
    },
    phone,
    favourite: false,
    course: speciality,
    bg_color: bgColor,
    note,
    picture: {}
  };

  console.log(newTeacher);
  return newTeacher;
};

function postTeacherToDB(e) {
  e.preventDefault();

  if (validateTeacher()) {
    const newTeacher = createTeacherData(addTeacherForm.fullname.value,
      addTeacherForm.speciality.value, addTeacherForm.country.value,
      addTeacherForm.city.value, addTeacherForm.email.value,
      addTeacherForm.telnum.value, addTeacherForm.birthDate.value,
      addTeacherForm.gender.value, addTeacherForm.color.value,
      addTeacherForm.comment.value);

    fetch('http://localhost:3000/teachers', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newTeacher)
    }).then(res => res.json())
      .then(data => console.log(data))
      .catch(error => console.log(`Something went wrong: ${error}`));

    teachersData.push(newTeacher);
    renderTopTeachers(teachersData, 1);
    renderStats(teachersData, 10, 1);
    renderChart(teachersData, 'course');
  }
}

addTeacherForm.addEventListener('submit', postTeacherToDB);
