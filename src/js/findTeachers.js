/* eslint-disable no-undef */
/* eslint-disable import/extensions */
// eslint-disable-next-line import/extensions
import { teachersData } from './formatTeachers.js';
import { openModalUser } from './renderTeachers.js';

const searchedList = document.getElementById('searched-list');
const searchInput = document.getElementById('search-input');
// eslint-disable-next-line arrow-body-style
const searchFunc = (searchingParameter) => {
  // eslint-disable-next-line max-len
  return _.filter(teachersData, (teacher) => _.includes(_.toLower(`${teacher.name.first} ${teacher.name.last}`), _.toLower(searchingParameter)) || (_.includes(_.toLower(teacher.note), _.toLower(searchingParameter)) || false) || (teacher.dob.age === _.parseInt(searchingParameter) || false));
};

searchInput.addEventListener('input', () => {
  searchedList.innerHTML = '';
  const cardHeightPX = 100;
  if (searchInput.value === '') {
    searchedList.style.maxHeight = '0';
    searchedList.innerHTML = '';
    return;
  }
  const searchResult = searchFunc(searchInput.value);
  _.map(searchResult, (teacher) => {
    let profilePic;
    if ('large' in teacher.picture) {
      profilePic = `<img class="searched-card-img" src="${teacher.picture.large}">`;
    } else {
      profilePic = `<div class="searched-card-initials d-flex"><p>${teacher.name.first[0]}. ${teacher.name.last[0]}.</p></div>`;
    }
    const teacherCard = document.createElement('div');
    teacherCard.classList.add('searched-card');
    teacherCard.classList.add('d-flex');
    teacherCard.innerHTML = `${profilePic}
    <section class="searched-card-info">
      <p>
        ${teacher.name.first} ${teacher.name.last}
      </p>
      <p>
        ${teacher.course}
      </p>
      <p>
        ${teacher.location.country}
      </p>
    </section>
    </div>`;

    teacherCard.addEventListener('click', () => {
      openModalUser(teacher);
    });

    searchedList.appendChild(teacherCard);
    return null;
  });
  searchedList.style.maxHeight = searchResult.length >= 3 ? `${3 * cardHeightPX}px` : `${searchResult.length * cardHeightPX}px`;
});
// eslint-disable-next-line max-len
const searchPercentage = (searchedData, generalData) => (searchedData.length / generalData.length) * 100;

console.log(searchPercentage(searchFunc('Math'), teachersData));
