/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable arrow-parens */
/* eslint-disable prefer-const */
/* eslint-disable no-undef */
/* eslint-disable no-param-reassign */
/* eslint-disable import/no-cycle */
/* eslint-disable no-use-before-define */
/* eslint-disable import/extensions */
import { filterData } from './filterTeachers.js';
import { renderFavourites } from './favourites.js';
import { renderStats } from './sortTeachers.js';

const teachersList = document.getElementById('teachers-list');
const pages = document.getElementById('paginationTeachers');

const modalUser = document.getElementById('modal-user');
const overlay = document.getElementsByClassName('overlay')[0];
const modalImage = document.getElementById('modal-img-wrap');
const fullName = document.getElementById('info-fullname');
const course = document.getElementById('info-course');
const location = document.getElementById('info-location');
const ageAndGender = document.getElementById('info-age-gender');
const email = document.getElementById('info-email');
const phone = document.getElementById('info-phone');
const note = document.getElementById('info-note');
const addToFav = document.getElementById('btn-add-fav');
const mapToggler = document.getElementById('mapToggler');
const mapContainer = document.getElementById('map');
const daysTillBdContainer = document.getElementById('days-till-bd');

const favIcon = ['<svg style="color: #f3da35" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-star-fill" viewBox="0 0 16 16"> <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" fill="#f3da35"></path> </svg>', '<svg style="color: #f3da35" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-star" viewBox="0 0 16 16"> <path d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.522-3.356c.33-.314.16-.888-.282-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288L8 2.223l1.847 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.565.565 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" fill="#f3da35"></path> </svg>'];
const changeFavState = (e) => {
  const teacher = e.currentTarget.targetTeacher;
  teacher.favourite = !teacher.favourite;
  addToFav.innerHTML = teacher.favourite ? favIcon[0] : favIcon[1];

  const teachersToRender = filterData();
  renderTopTeachers(teachersToRender, currentPage);
  renderStats(teachersToRender, 10, 1);
  renderFavourites(teachersToRender);
};

let map;
const openMap = async (e) => {
  const teacher = e.currentTarget.targetTeacher;
  let latitude;
  let longitude;

  if (mapContainer.style.display === 'block') {
    mapContainer.style.display = 'none';
    return;
  }
  if (map !== undefined) {
    map.off();
    map.remove();
  }
  if ('coordinates' in teacher.location) {
    latitude = teacher.location.coordinates.latitude;
    longitude = teacher.location.coordinates.longitude;
  } else {
    let coordinates = (await fetchCoordinates(teacher.location.city))[0];
    latitude = coordinates.lat;
    longitude = coordinates.lon;
  }

  mapContainer.style.display = 'block';

  map = L.map('map').setView([latitude, longitude], 13);
  L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
  }).addTo(map);
  let marker = new L.Marker([latitude, longitude]);
  marker.addTo(map);
};

async function fetchCoordinates(cityName) {
  const response = await fetch(`https://nominatim.openstreetmap.org/search?format=json&limit=3&q=${cityName}`);
  const data = await response.json();
  console.log(data);
  return data;
}

const calculateDaysTillBd = (birthdayDate) => {
  let date2 = _.now();
  let date1 = new Date(birthdayDate);
  date1 = dayjs(birthdayDate).year(dayjs().year());
  if (dayjs(date1).isBefore(dayjs(date2)) === true) {
    date1 = dayjs(date1).year(dayjs().year() + 1);
  }
  return date1.diff(date2, 'day') + 1;
};

export const openModalUser = (teacher) => {
  let imageSrc;
  if ('large' in teacher.picture) {
    imageSrc = teacher.picture.large;
  } else {
    imageSrc = 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png';
  }

  addToFav.innerHTML = teacher.favourite ? favIcon[0] : favIcon[1];
  addToFav.targetTeacher = teacher;
  addToFav.addEventListener('click', changeFavState);

  mapToggler.targetTeacher = teacher;
  mapToggler.addEventListener('click', openMap);

  modalImage.innerHTML = `<img class="modal-image floatl" style="${'bg_color' in teacher ? `border: 10px solid ${teacher.bg_color}` : 'border: 10px solid #f86c5b'}" src="${imageSrc}">`;
  fullName.innerHTML = `${teacher.name.first} ${teacher.name.last}`;
  course.innerHTML = teacher.course;
  location.innerHTML = `${teacher.location.city}, ${teacher.location.country}`;
  ageAndGender.innerHTML = `${teacher.dob.age}, ${teacher.gender}`;
  daysTillBdContainer.innerHTML = `${calculateDaysTillBd(teacher.dob.date)} days till birthday!`;
  email.innerHTML = teacher.email;
  phone.innerHTML = teacher.phone;
  note.innerHTML = teacher.note;

  modalUser.classList.remove('hidden');
  overlay.classList.remove('hidden');
};

let currentPage = 1;
const pageSize = 10;

export function renderTopTeachers(renderData, pageCurrent) {
  teachersList.innerHTML = '';
  currentPage = pageCurrent;
  renderTeachersPage();
  const startIndex = (pageCurrent - 1) * pageSize;
  const endIndex = startIndex + pageSize;
  if (renderData.length === 0) {
    teachersList.innerHTML = '<p class="no-data-message">Sorry, no teachers were found:(</p>';
    return;
  }

  for (let i = startIndex; i < endIndex && i < renderData.length; i += 1) {
    const teacher = renderData[i];
    let profilePic;
    if ('large' in teacher.picture) {
      profilePic = `<img src="${teacher.picture.large}">`;
    } else {
      profilePic = `<p class="initials d-flex">${teacher.name.first[0]}. ${teacher.name.last[0]}.</p>`;
    }
    const teacherCard = document.createElement('div');
    teacherCard.classList.add('teacher-card');
    teacherCard.classList.add(`${teacher.favourite ? 'favourite' : null}`);
    teacherCard.classList.add('floatl');

    teacherCard.innerHTML = `<div class="card-img">
    <div class="img-wrapper d-flex">
        ${profilePic}
    </div>
    </div>
    <section class="teacher-info">
        <div><p>${teacher.name.first}</p><p>${teacher.name.last}</p></div>
        <div>${teacher.course ? teacher.course : ''}</div>
        <div>${teacher.location.country ? teacher.location.country : ''}</div>
    </section>
    </div>`;

    teacherCard.addEventListener('click', () => {
      openModalUser(teacher);
    });

    teachersList.appendChild(teacherCard);
  }
}

export const renderTeachersPage = () => {
  pages.innerHTML = '';
  const pageCount = _.ceil(filterData().length / pageSize);

  for (let i = 1; i <= pageCount; i += 1) {
    if (i === 1 || i === pageCount) {
      const pageBtn = document.createElement('button');
      pageBtn.classList.add('btn-table');
      pageBtn.innerHTML = i;
      pageBtn.addEventListener('click', () => {
        renderTopTeachers(filterData(), i);
      });
      pages.appendChild(pageBtn);
    } else if (i === currentPage || i === currentPage + 1 || i === currentPage - 1) {
      const pageBtn = document.createElement('button');
      pageBtn.classList.add('btn-table');
      pageBtn.innerHTML = i;
      pageBtn.addEventListener('click', () => {
        renderTopTeachers(filterData(), i);
      });
      pages.appendChild(pageBtn);
    } else if (i === currentPage + 2 || i === currentPage - 2) {
      const dot = '<span class="table-dots">&hellip;</span>';
      pages.insertAdjacentHTML('beforeend', dot);
    }
  }
};

export function renderTeachers(renderData, parent) {
  parent.innerHTML = '';

  if (renderData.length === 0) {
    parent.innerHTML = '<p class="no-data-message">Sorry, no teachers were found:(</p>';
    return;
  }

  _.map(renderData, (teacher) => {
    let profilePic;
    if ('large' in teacher.picture) {
      profilePic = `<img src="${teacher.picture.large}">`;
    } else {
      profilePic = `<p class="initials d-flex">${teacher.name.first[0]}. ${teacher.name.last[0]}.</p>`;
    }
    const teacherCard = document.createElement('div');
    teacherCard.classList.add('teacher-card');
    teacherCard.classList.add(`${teacher.favourite ? 'favourite' : null}`);
    teacherCard.classList.add('floatl');

    teacherCard.innerHTML = `<div class="card-img">
    <div class="img-wrapper d-flex">
        ${profilePic}
    </div>
    </div>
    <section class="teacher-info">
        <div><p>${teacher.name.first}</p><p>${teacher.name.last}</p></div>
        <div>${teacher.course ? teacher.course : ''}</div>
        <div>${teacher.location.country ? teacher.location.country : ''}</div>
    </section>
    </div>`;

    teacherCard.addEventListener('click', () => {
      openModalUser(teacher);
    });

    parent.appendChild(teacherCard);
    return null;
  });
}
