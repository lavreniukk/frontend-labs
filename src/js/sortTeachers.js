/* eslint-disable no-undef */
/* eslint-disable no-new */
/* eslint-disable import/no-cycle */
/* eslint-disable quotes */
/* eslint-disable no-loop-func */
/* eslint-disable max-len */
/* eslint-disable import/extensions */
import { filterData } from "./filterTeachers.js";

const resetBtn = document.getElementById('btn-reset');
const statsTableBody = document.getElementById('stats-table-body');
const pages = document.getElementById('paginationStats');
const chartCanvas = document.getElementById('chart');
const statsSelect = document.getElementById('statsField');

let copyForSorting = [];
let sortAscend = false;
let thisSort;

let currentPage = 1;
const pageSize = 10;

const renderStats = (renderData, maxPerPage, pageCurrent) => {
  if (copyForSorting !== renderData) {
    copyForSorting = [...renderData];
  }
  statsTableBody.innerHTML = '';
  currentPage = pageCurrent;
  // eslint-disable-next-line no-use-before-define
  renderPaginationStats();
  const startIndex = (pageCurrent - 1) * maxPerPage;
  const endIndex = startIndex + maxPerPage;

  for (let i = startIndex; i < endIndex && i < renderData.length; i += 1) {
    const teacher = renderData[i];

    const tableRow = `<tr>
    <td>${teacher.name.first} ${teacher.name.last}</td>
    <td>${teacher.course ? teacher.course : 'Unknown'}</td>
    <td>${teacher.dob.age ? teacher.dob.age : 'Unknown'}</td>
    <td>${teacher.gender}</td>
    <td>${teacher.location.country ? teacher.location.country : 'Unknown'}</td>
    </tr>`;

    statsTableBody.insertAdjacentHTML('beforeend', tableRow);
  }
};

const renderPaginationStats = () => {
  pages.innerHTML = '';
  const pageCount = _.ceil(copyForSorting.length / pageSize);
  for (let i = 1; i <= pageCount; i += 1) {
    if (i === 1 || i === pageCount) {
      const pageBtn = document.createElement('button');
      pageBtn.classList.add('btn-table');
      pageBtn.innerHTML = i;
      pageBtn.addEventListener('click', () => {
        renderStats(copyForSorting, pageSize, i);
      });
      pages.appendChild(pageBtn);
    } else if (i === currentPage || i === currentPage + 1 || i === currentPage - 1) {
      const pageBtn = document.createElement('button');
      pageBtn.classList.add('btn-table');
      pageBtn.innerHTML = i;
      pageBtn.addEventListener('click', () => {
        renderStats(copyForSorting, pageSize, i);
      });
      pages.appendChild(pageBtn);
    } else if (i === currentPage + 2 || i === currentPage - 2) {
      const dot = '<span class="table-dots">&hellip;</span>';
      pages.insertAdjacentHTML('beforeend', dot);
    }
  }
};

const sortTeachers = (e) => {
  const sortProperty1 = e.target.dataset.sort1;
  const sortProperty2 = e.target.dataset.sort2;

  if (`${sortProperty1} ${sortProperty2}` === thisSort) sortAscend = !sortAscend;
  thisSort = `${sortProperty1} ${sortProperty2}`;

  if (sortAscend) {
    if (sortProperty2 === undefined) {
      copyForSorting.sort((firstTeacher, secondTeacher) => (firstTeacher[sortProperty1] > secondTeacher[sortProperty1] ? 1 : -1));
    } else {
      copyForSorting.sort((firstTeacher, secondTeacher) => (firstTeacher[sortProperty1][sortProperty2] > secondTeacher[sortProperty1][sortProperty2] ? 1 : -1));
    }
    renderStats(copyForSorting, pageSize, currentPage);
  }

  if (!sortAscend) {
    if (sortProperty2 === undefined) {
      copyForSorting.sort((firstTeacher, secondTeacher) => (firstTeacher[sortProperty1] < secondTeacher[sortProperty1] ? 1 : -1));
    } else {
      copyForSorting.sort((firstTeacher, secondTeacher) => (firstTeacher[sortProperty1][sortProperty2] < secondTeacher[sortProperty1][sortProperty2] ? 1 : -1));
    }
    renderStats(copyForSorting, pageSize, currentPage);
  }

  return [];
};

// eslint-disable-next-line import/prefer-default-export
export const resetSorting = () => {
  sortAscend = false;
  thisSort = '';
  copyForSorting = filterData();
  renderStats(copyForSorting, pageSize, currentPage);
};

resetBtn.addEventListener('click', resetSorting);

document.querySelectorAll('#stats-table thead tr th').forEach((column) => {
  column.addEventListener('click', sortTeachers);
});

let chart;
export const renderChart = (renderData, sortByKey = 'course') => {
  if (chart) {
    chart.destroy();
  }
  const freqMap = new Map();

  if (renderData[0][sortByKey] !== undefined) {
    _.forEach(renderData, (teacher) => {
      if (freqMap.has(teacher[sortByKey])) {
        const value = freqMap.get(teacher[sortByKey]);
        freqMap.set(teacher[sortByKey], value + 1);
      } else {
        freqMap.set(teacher[sortByKey], 1);
      }
    });
  } else {
    const firstKey = _.split(sortByKey, '.', 2)[0];
    const secondKey = _.split(sortByKey, '.', 2)[1];

    _.forEach(renderData, (teacher) => {
      if (freqMap.has(teacher[firstKey][secondKey])) {
        const value = freqMap.get(teacher[firstKey][secondKey]);
        freqMap.set(teacher[firstKey][secondKey], value + 1);
      } else {
        freqMap.set(teacher[firstKey][secondKey], 1);
      }
    });
  }

  chart = new Chart(chartCanvas, {
    type: 'pie',
    data: {
      labels: Array.from(freqMap.keys()),
      datasets: [{
        label: 'Teachers count:',
        data: Array.from(freqMap.values()),
        hoverOffset: 4,
      }],
    },
  });
};

const changeSortingForChart = () => {
  renderChart(filterData(), statsSelect.value);
};
statsSelect.addEventListener('change', changeSortingForChart);

export { renderStats, renderPaginationStats };
