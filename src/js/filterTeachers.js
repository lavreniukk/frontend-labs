/* eslint-disable no-undef */
/* eslint-disable import/no-cycle */
/* eslint-disable import/extensions */
// eslint-disable-next-line import/no-cycle
import { renderTopTeachers } from './renderTeachers.js';
import { teachersData } from './formatTeachers.js';
import { countriesList } from './countriesList.js';
import { renderChart, renderStats } from './sortTeachers.js';

const ageFilter = document.getElementById('age');
const regionFilter = document.getElementById('region');
const genderFilter = document.getElementById('sex');
const photoFilter = document.getElementById('photo-check');
const favouriteFilter = document.getElementById('favourite-check');

const filters = { };
const getMinAge = (age) => _.split(age.toString(), '-')[0];
const getMaxAge = (age) => _.split(age.toString(), '-')[1];

// eslint-disable-next-line arrow-body-style, import/prefer-default-export
export const filterData = () => {
  return _.filter(teachersData, (teacher) => _.every(Object.keys(filters), (key) => {
    if (filters[key] === 'Any') {
      return true;
    }

    if (key === 'picture') {
      return teacher.picture.large !== undefined;
    }

    if (key === 'age') {
      return teacher.dob[key] >= getMinAge(filters[key])
          && teacher.dob[key] <= getMaxAge(filters[key]);
    }

    if (key === 'country') {
      // eslint-disable-next-line max-len
      return _.find(countriesList, (element) => element.country === teacher.location[key] && element.location === filters[key]);
    }

    return _.toLower(filters[key]?.toString()) === _.toLower(teacher[key]?.toString());
  }));
};

ageFilter.addEventListener('change', () => {
  filters.age = ageFilter.value;
  renderTopTeachers(filterData(), 1);
  renderStats(filterData(), 10, 1);
  renderChart(filterData());

  console.log(filterData());
  console.log(filters);
});

regionFilter.addEventListener('change', () => {
  filters.country = regionFilter.value;
  renderTopTeachers(filterData(), 1);
  renderStats(filterData(), 10, 1);
  renderChart(filterData());

  console.log(filterData());
  console.log(filters);
});

genderFilter.addEventListener('change', () => {
  filters.gender = genderFilter.value;
  renderTopTeachers(filterData(), 1);
  renderStats(filterData(), 10, 1);
  renderChart(filterData());

  console.log(filterData());
  console.log(filters);
});

photoFilter.addEventListener('change', () => {
  if (photoFilter.checked) {
    filters.picture = photoFilter.checked;
  } else {
    delete filters.picture;
  }
  renderTopTeachers(filterData(), 1);
  renderStats(filterData(), 10, 1);
  renderChart(filterData());

  console.log(filterData());
  console.log(filters);
});

favouriteFilter.addEventListener('change', () => {
  if (favouriteFilter.checked) {
    filters.favourite = favouriteFilter.checked;
  } else {
    delete filters.favourite;
  }
  renderTopTeachers(filterData(), 1);
  renderStats(filterData(), 10, 1);
  renderChart(filterData());

  console.log(filterData());
  console.log(filters);
});
