const modalForm = document.getElementById('modal-form');
const modal = document.getElementsByClassName('modal');
const overlay = document.getElementsByClassName('overlay')[0];
const mapContainer = document.getElementById('map');

// eslint-disable-next-line no-unused-vars
const openModalForm = () => {
  modalForm.classList.remove('hidden');
  overlay.classList.remove('hidden');
};

// eslint-disable-next-line no-unused-vars
const closeModal = () => {
  for (let i = 0; i < modal.length; i += 1) {
    if (!modal[i].classList.contains('hidden')) {
      modal[i].classList.add('hidden');
      break;
    }
  }
  mapContainer.style.display = 'none';
  overlay.classList.add('hidden');
};

document.querySelectorAll('.nav-links button').forEach((button) => {
  button.addEventListener('click', openModalForm);
});

document.querySelectorAll('.modal-header button').forEach((button) => {
  button.addEventListener('click', closeModal);
});
