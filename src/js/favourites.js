/* eslint-disable no-use-before-define */
/* eslint-disable no-param-reassign */
/* eslint-disable import/prefer-default-export */
// eslint-disable-next-line import/extensions, import/no-cycle
import { renderTeachers } from './renderTeachers.js';

const cardsWrap = document.getElementById('cards-wrapper');
const carouselBody = document.getElementById('carousel-body');
const leftArrow = document.getElementById('carousel-favourites-left');
const rightArrow = document.getElementById('carousel-favourites-right');
let cards = carouselBody.querySelectorAll('.teacher-card');

export const renderFavourites = (renderData) => {
  const favouriteTeachers = renderData.filter((teacher) => teacher.favourite);
  renderTeachers(favouriteTeachers, carouselBody);
  cards = carouselBody.querySelectorAll('.teacher-card');
  setStylesForCards();
};

let translateX = 0;
let cardsShown = 5;
let cardIndex = cardsShown;

const getCardWidth = () => {
  const card = cards[0];
  const cardStyles = window.getComputedStyle(card);
  const width = card.offsetWidth + parseFloat(cardStyles.marginLeft)
  + parseFloat(cardStyles.marginRight) + parseFloat(cardStyles.paddingRight)
  + parseFloat(cardStyles.paddingLeft);
  return width;
};

const showPrevCard = () => {
  if (cardIndex === cardsShown) return;
  cardIndex -= 1;
  translateX += getCardWidth();
  carouselBody.style.transform = `translateX(${translateX}px)`;
};

const showNextCard = () => {
  if (cardIndex === cards.length) return;
  cardIndex += 1;
  translateX -= getCardWidth();
  carouselBody.style.transform = `translateX(${translateX}px)`;
};

let cardWidth;
let contWidth;
let marginCount;

const setStylesForCards = () => {
  if (cards.length !== 0) {
    cardWidth = parseFloat(window.getComputedStyle(cards[0]).width);
    contWidth = parseFloat(window.getComputedStyle(cardsWrap).width);
  } else {
    return;
  }

  if (contWidth >= 700) {
    cardsShown = 5;
  } else if (contWidth < 700 && contWidth > 600) {
    cardsShown = 4;
  } else if (contWidth <= 600 && contWidth > 500) {
    cardsShown = 3;
  } else if (contWidth <= 500) {
    cardsShown = 1;
  }

  carouselBody.style.transform = 'translateX(0px)';
  translateX = 0;
  cardIndex = cardsShown;
  // eslint-disable-next-line max-len
  marginCount = 2 * cardsShown;
  const margin = (contWidth - (cardsShown * cardWidth)) / marginCount;
  cards.forEach((card) => {
    card.style.marginRight = `${margin}px`;
    card.style.marginLeft = `${margin}px`;
  });
};

// eslint-disable-next-line no-restricted-globals
addEventListener('resize', setStylesForCards);
leftArrow.addEventListener('click', showPrevCard);
rightArrow.addEventListener('click', showNextCard);
