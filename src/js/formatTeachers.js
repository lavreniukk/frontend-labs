/* eslint-disable no-undef */
/* eslint-disable no-param-reassign */
/* eslint-disable import/no-mutable-exports */
/* eslint-disable import/no-cycle */
/* eslint-disable import/extensions */
import baseURL from './baseUrl.js';
import { renderTopTeachers } from './renderTeachers.js';
import { renderFavourites } from './favourites.js';
import { renderStats, renderChart } from './sortTeachers.js';

const loadMoreBtn = document.getElementById('btn-loadmore');
const courses = ['Mathematics', 'Physics', 'English', 'Computer Science', 'Dancing', 'Chess', 'Biology', 'Chemistry', 'Law', 'Art', 'Medicine', 'Statistics'];
let teachersData = [];

const getRandomColor = () => {
  const letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i += 1) {
    color += letters[_.floor(Math.random() * letters.length)];
  }
  return color;
};

const teacherCount = 50;
const teachersOnStatsPage = 10;

async function fetachFromDB() {
  const response = await fetch('http://localhost:3000/teachers');

  if (response.status === 200) {
    const teachers = await response.json();
    teachersData = _.concat(teachersData, teachers);
    renderTopTeachers(teachersData, 1);
    renderFavourites(teachersData);
    renderStats(teachersData, teachersOnStatsPage, 1);
    renderChart(teachersData);
  }
}

async function fetchData() {
  const response = await fetch(`${baseURL}?results=${teacherCount}`);

  if (response.status === 200) {
    let teachers = await response.json();
    teachers = teachers.results;

    teachers.map((teacher) => {
      teacher.bg_color = getRandomColor();
      teacher.course = courses[Math.floor(Math.random() * courses.length)];
      teacher.note = teacher.course;
      teacher.favourite = Math.random() >= 0.5;
      return teacher;
    });

    teachersData = _.concat(teachersData, teachers);
    renderTopTeachers(teachersData, 1);
    renderFavourites(teachersData);
    renderStats(teachersData, teachersOnStatsPage, 1);
    renderChart(teachersData);
  }
}
fetachFromDB();
fetchData();

async function fetchMoreData() {
  const response = await fetch(`${baseURL}?results=10`);

  if (response.status === 200) {
    let moreTeachers = await response.json();
    moreTeachers = moreTeachers.results;

    moreTeachers.map((teacher) => {
      teacher.bg_color = getRandomColor();
      teacher.course = courses[Math.floor(Math.random() * courses.length)];
      teacher.note = teacher.course;
      teacher.favourite = Math.random() >= 0.5;
      return teacher;
    });

    teachersData = _.concat(teachersData, moreTeachers);
    renderTopTeachers(teachersData, 1);
    renderFavourites(teachersData);
    renderStats(teachersData, teachersOnStatsPage, 1);
    renderChart(teachersData);
  }
}

loadMoreBtn.addEventListener('click', fetchMoreData);

export const pushToTeachers = (teacher) => {
  teachersData.push(teacher);
};
// teachersData = randomUserMock.map(({
//   gender, name, location, email, dob, phone, picture,
// }) => {
//   const id = counter; counter += 1;
//   const { title } = name;
//   const full_name = `${name.first} ${name.last}`;
//   // eslint-disable-next-line object-curly-newline
//   const { city, state, country, postcode, coordinates, timezone } = location;
//   const b_date = dob.date;
//   const { age } = dob;
//   const picture_large = picture.large;
//   const picture_thumbnail = picture.thumbnail;
//   const favourite = Math.random() <= 0.5;
//   const course = courses[Math.floor(Math.random() * courses.length)];
//   const bg_color = getRandomColor();
//   const note = course;
//   return ({
//     id,
//     gender,
//     title,
//     full_name,
//     city,
//     state,
//     country,
//     postcode,
//     coordinates,
//     timezone,
//     email,
//     b_date,
//     age,
//     phone,
//     picture_large,
//     picture_thumbnail,
//     favourite,
//     course,
//     bg_color,
//     note,
//   });
// });

// teachersData = teachersData.concat(additionalUsers);
// const fName = teachersData.map((teacher) => teacher.full_name);
// eslint-disable-next-line max-len, max-len
// teachersData = teachersData.filter((teacher, index) => fName.indexOf(teacher.full_name) === index);
// eslint-disable-next-line import/prefer-default-export
export { teachersData, fetchData };
